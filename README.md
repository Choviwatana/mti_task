# MTI Summer Internship Assignment

[素のHTML, CSS, JavaScriptで0から始めるWeb開発](http://tech.mti.co.jp/entry/2017/07/21/573/) による事前課題に取り組むプロジェクトです。

## Getting Started

提供されているコードに気にしなければいけないバッグがあるということで、Git管理の上でデバッグしていきます。

デバッグ・実装する点として以下に列挙しました。

### 気づいたバッグ

- Game Overの判定がおかしい
- Restart処理がない
- 左右に壁に当たるとブロックが変形する
- DeleteRow関数のバッグ

### 個人的に欲しい機能

- MoveDown関数
- Scoreの計算
- ブロック回転関数