document.getElementById("hello_text").textContent = "はじめてのJavaScript";

var count = 0;
var rowNum = 20;
var colNum = 10;
var score = 0;
var scoreStep = 40;
var cells;

// ブロックのパターン
var blocks = {
    i: {
        class: "i",
        pattern: [
            [1, 1, 1, 1]
        ]
    },
    o: {
        class: "o",
        pattern: [
            [1, 1],
            [1, 1]
        ]
    },
    t: {
        class: "t",
        pattern: [
            [0, 1, 0],
            [1, 1, 1]
        ]
    },
    s: {
        class: "s",
        pattern: [
            [0, 1, 1],
            [1, 1, 0]
        ]
    },
    z: {
        class: "z",
        pattern: [
            [1, 1, 0],
            [0, 1, 1]
        ]
    },
    j: {
        class: "j",
        pattern: [
            [1, 0, 0],
            [1, 1, 1]
        ]
    },
    l: {
        class: "l",
        pattern: [
            [0, 0, 1],
            [1, 1, 1]
        ]
    }
};

loadTable();
var isGameOver = false;
gameInterval = setInterval(function() {
    // スコア更新
    document.getElementById("score_text").textContent = "Score: " + (count + score * scoreStep).toString();
    count++;
    // ブロックが積み上がり切っていないかのチェック
    for (var row = 0; !isGameOver && row < 2; row++) {
        for (var col = 0; !isGameOver && col < 10; col++) {
            if (cells[row][col].className !== "" && cells[row][col].blockNum !== fallingBlockNum) {
                isGameOver = true;
                var comfirmDiag = confirm("Game Over! You get " + (count + score * scoreStep) + " points.\n Do you want to restart?");
                if (comfirmDiag == true) { // restart
                    console.log("You pressed OK!");
                    location.reload();
                }
            }
        }
    }
    if (isGameOver) {
        clearInterval(gameInterval);
        document.getElementById("hello_text").textContent = "Game Over! You get " + (count + score * scoreStep) + " points.";
        console.log("cleared");
    } else {
        if (hasFallingBlock()) { // 落下中のブロックがあるか確認する
            fallBlocks(); // あればブロックを落とす
        } else { // なければ
            deleteRow(); // そろっている行を消す
            generateBlock(); // ランダムにブロックを作成する
        }
    }
}, 1000);

// キーボードイベントを監視する
document.addEventListener("keydown", onKeyDown);

// キー入力によってそれぞれの関数を呼び出す
function onKeyDown(event) {
    if (event.keyCode === 37) {
        moveLeft();
    } else if (event.keyCode === 39) {
        moveRight();
    } else if (event.keyCode === 40) {
        fallBlocks();
    } else if (event.keyCode === 38) {
        rotateBlock();
    }
}

/* ------ ここから下は関数の宣言部分 ------ */

function loadTable() {
    cells = [];
    var td_array = document.getElementsByTagName("td");
    var index = 0;
    for (var row = 0; row < rowNum; row++) {
        cells[row] = [];
        for (var col = 0; col < colNum; col++) {
            cells[row][col] = td_array[index];
            index++;
        }
    }
}

function fallBlocks() {
    // 1. 底についていないか？
    for (var col = 0; col < colNum; col++) {
        if (cells[rowNum - 1][col].blockNum === fallingBlockNum) {
            isFalling = false;
            return; // 一番下の行にブロックがいるので落とさない
        }
    }
    // 2. 1マス下に別のブロックがないか？
    for (var row = rowNum - 2; row >= 0; row--) {
        for (var col = 0; col < colNum; col++) {
            if (cells[row][col].blockNum === fallingBlockNum) {
                if (cells[row + 1][col].className !== "" && cells[row + 1][col].blockNum !== fallingBlockNum) {
                    isFalling = false;
                    return; // 一つ下のマスにブロックがいるので落とさない
                }
            }
        }
    }
    // 下から二番目の行から繰り返しクラスを下げていく
    for (var row = rowNum - 2; row >= 0; row--) {
        for (var col = 0; col < colNum; col++) {
            if (cells[row][col].blockNum === fallingBlockNum) {
                cells[row + 1][col].className = cells[row][col].className;
                cells[row + 1][col].blockNum = cells[row][col].blockNum;
                cells[row][col].className = "";
                cells[row][col].blockNum = null;
            }
        }
    }
}

var isFalling = false;

function hasFallingBlock() {
    // 落下中のブロックがあるか確認する
    return isFalling;
}

function deleteRow() {
    // そろっている行を消す
    for (var row = rowNum - 1; row >= 0; row--) {
        var canDelete = true;
        for (var col = 0; col < colNum; col++) {
            if (cells[row][col].className === "") {
                canDelete = false;
            }
        }
        if (canDelete) {
            console.log("" + row);
            score += 1;
            // 1行消す
            for (var col = 0; col < colNum; col++) {
                cells[row][col].className = "";
            }
            // 上の行のブロックをすべて1マス落とす
            for (var downRow = row - 1; downRow >= 0; downRow--) {
                for (var col = 0; col < colNum; col++) {
                    console.log("" + downRow);
                    cells[downRow + 1][col].className = cells[downRow][col].className;
                    cells[downRow + 1][col].blockNum = cells[downRow][col].blockNum;
                    cells[downRow][col].className = "";
                    cells[downRow][col].blockNum = null;
                }
            }
            row++;  // 消した行の次の行も揃っているかを確認するために
        }
    }
}

var fallingBlockNum = 0;
var blockPattern = null;

function generateBlock() {
    // ランダムにブロックを生成する
    // 1. ブロックパターンからランダムに一つパターンを選ぶ
    var keys = Object.keys(blocks);
    var nextBlockKey = keys[Math.floor(Math.random() * keys.length)];
    var nextBlock = blocks[nextBlockKey];
    var nextFallingBlockNum = fallingBlockNum + 1;
    // 2. 選んだパターンをもとにブロックを配置する
    blockPattern = nextBlock.pattern;
    for (var row = 0; row < blockPattern.length; row++) {
        for (var col = 0; col < blockPattern[row].length; col++) {
            if (blockPattern[row][col]) { // ゲーム盤の中央上部に登場するよう左から4番目のマスから配置
                cells[row][col + 3].className = nextBlock.class;
                cells[row][col + 3].blockNum = nextFallingBlockNum;
            }
        }
    }
    // 3. 落下中のブロックがあるとする
    isFalling = true;
    fallingBlockNum = nextFallingBlockNum;
}

function moveRight() {
    // 1. 壁についていないか？
    for (var row = 0; row < rowNum; row++) {
        if (cells[row][colNum - 1].blockNum === fallingBlockNum) {
            return; // 一番右の行に壁がいるので移動できない
        }
    }
    // 2. 1マス右に別のブロックがないか？
    for (var row = 0; row < rowNum; row++) {
        for (var col = colNum - 1; col >= 0; col--) {
            if (cells[row][col].blockNum === fallingBlockNum) {
                if (cells[row][col + 1].className !== "" && cells[row][col + 1].blockNum !== fallingBlockNum) {
                    return; // 一つ右のマスにブロックがいるので移動させない
                }
            }
        }
    }
    // 3. ブロックを右に移動させる
    for (var row = 0; row < rowNum; row++) {
        for (var col = colNum - 1; col >= 0 && hasFallingBlock(); col--) {
            if (cells[row][col].blockNum === fallingBlockNum) {
                cells[row][col + 1].className = cells[row][col].className;
                cells[row][col + 1].blockNum = cells[row][col].blockNum;
                cells[row][col].className = "";
                cells[row][col].blockNum = null;
            }
        }
    }
}

function moveLeft() {
    // 1. 壁についていないか？
    for (var row = 0; row < rowNum; row++) {
        if (cells[row][0].blockNum === fallingBlockNum) {
            return; // 一番右の行に壁がいるので移動できない
        }
    }
    // 2. 1マス左に別のブロックがないか？
    for (var row = 0; row < rowNum; row++) {
        for (var col = 1; col < colNum; col++) {
            if (cells[row][col].blockNum === fallingBlockNum) {
                if (cells[row][col - 1].className !== "" && cells[row][col - 1].blockNum !== fallingBlockNum) {
                    return; // 一つ右のマスにブロックがいるので移動させない
                }
            }
        }
    }
    // ブロックを左に移動させる
    for (var row = 0; row < rowNum; row++) {
        for (var col = 0; col < colNum && hasFallingBlock(); col++) {
            if (cells[row][col].blockNum === fallingBlockNum) {
                cells[row][col - 1].className = cells[row][col].className;
                cells[row][col - 1].blockNum = cells[row][col].blockNum;
                cells[row][col].className = "";
                cells[row][col].blockNum = null;
            }
        }
    }
}

function rotateBlock() {
    // ブロックのいる場所を確認する
    var blockRow = 0,
        blockCol = 0;
    for (var col = 0; col < colNum; col++) {
        for (var row = 0; row < rowNum; row++) {
            if (cells[row][col].blockNum === fallingBlockNum) {
                blockRow = row, blockCol = col;
                row = rowNum, col = colNum;
            }
        }
    }
    var className = cells[blockRow][blockCol].className // 回転後に代入用
    var blockNum = cells[blockRow][blockCol].blockNum


    if (!hasFallingBlock()) {
        return;
    }
    for (var row = 0; row < blockPattern[0].length; row++) {
        for (var col = 0; col < blockPattern.length; col++) {
            if (cells[blockRow + row][blockCol + col].className !== "" && cells[blockRow + row][blockCol + col].blockNum !== fallingBlockNum) {
                return; // はまらないので回転できない
            }
        }
    }
    // 回転前のブロックを消す
    for (var row = 0; row < rowNum; row++) {
        for (var col = 0; col < colNum; col++) {
            if (cells[row][col].blockNum === fallingBlockNum) { // ゲーム盤の中央上部に登場するよう左から4番目のマスから配置
                cells[row][col].className = "";
                cells[row][col].blockNum = null;
            }
        }
    }
    // 落ちているブロックのパターンを右回りに回転する
    rotatePattern = new Array();
    for (var row = 0; row < blockPattern[0].length; row++) {
        rotatePattern[row] = new Array();
        for (var col = 0; col < blockPattern.length; col++) {
            rotatePattern[row][col] = blockPattern[blockPattern.length - col - 1][row];
        }
    }
    // 落下中なら回転後のブロックを入れる
    for (var row = 0; row < rotatePattern.length; row++) {
        for (var col = 0; col < rotatePattern[row].length; col++) {
            if (rotatePattern[row][col]) { // ゲーム盤の中央上部に登場するよう左から4番目のマスから配置
                cells[row + blockRow][col + blockCol].className = className;
                cells[row + blockRow][col + blockCol].blockNum = blockNum;
            }
        }
    }
    blockPattern = rotatePattern;
}